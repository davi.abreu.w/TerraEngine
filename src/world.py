import time
import random
from enum import Enum

class Resource(Enum):
    GRASS = 1
    ORE = 2
    WATER = 3
    WORKSHOP = 4


class Cood():

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, other):
        if isinstance(other, Cood):
            return Cood(self.x + other.x, self.y + other.y)
        return None

    def __str__(self):
        return "{}-{}".format(self.x, self.y)

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        if isinstance(other, Cood):
            return self.__str__() == other.__str__()
        raise TypeError("Trying to compare Cood with not Cood instance: `{}` {}".format(other, type(other)))

    def __ne__(self, other):
            return not (self == other)

    def __hash__(self):
      return hash((self.x, self.y))


class TerrainMap():

    def __init__(self, map_name):
        self.heigth = None
        self.width = None
        self.terrains = {}
        self.load(map_name)

    def extract_size(self, map_string_list):
        self.heigth = len(map_string_list)
        if self.heigth:
            self.width = len(map_string_list[0])

    def build(self, map_string_list):
        for y in range(self.heigth):
            for x in range(self.width):
                tile_char = int(map_string_list[y][x])
                self.terrains[Cood(x, y)] = Resource(tile_char)

    def load(self, map_name):
        map_string_list = [item.replace('\n', '') for item in open(map_name).readlines()]
        self.extract_size(map_string_list)
        self.build(map_string_list)

    def choose_grassland(self):
        while(True):
            x = random.randint(0, self.width - 1)
            y = random.randint(0, self.heigth - 1)
            if self.terrains[Cood(x, y)] is Resource.GRASS:
                return Cood(x, y)

    def terrain_info(self, cood):
        return self.terrains.get(cood)


class Adam():

    MOVE_RATE = 2
    CANNOT_MOVE_LIST = [Resource.WATER]

    def __init__(self, tmap):
        self.move_stats = self.build_move_states()
        self.tmap = tmap
        self.TARGET = Resource.ORE
        self.location = self.tmap.choose_grassland()

    def build_move_states(self):
        move_stats = []
        for x in range(-self.MOVE_RATE, self.MOVE_RATE + 1):
            for y in range(-self.MOVE_RATE, self.MOVE_RATE + 1):
                move_stats.append(Cood(x, y))
        return move_stats

    def calculate_possible_moves(self):
        move_list = []
        for delta_cood in self.move_stats:
            possible_move = self.location + delta_cood
            terrain = self.tmap.terrain_info(possible_move)
            if terrain is not None:
                if terrain not in self.CANNOT_MOVE_LIST:
                    move_list.append(possible_move)
        return move_list

    def move(self):
        move_list = self.calculate_possible_moves()
        next_location = random.choice(move_list)
        terrain = self.tmap.terrain_info(self.location)
        next_terrain = self.tmap.terrain_info(next_location)
        print("Moving: `{}` ({}) -> `{}` ({})".format(self.location, terrain, next_location, next_terrain))
        self.location = next_location


# tmap = TerrainMap('world')
# actor = Adam(tmap)
# while True:
#     actor.move()
#     time.sleep(1)